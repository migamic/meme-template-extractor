import os
import numpy as np
import torch
from torchvision.io import read_image
from torch.utils.data import Dataset
import glob


# set random state for reproducibility and set up random number generator
def set_seed(seed=2021):
    rng = np.random.RandomState(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    return rng


# Custom transform to scale images to have pixels in range [-1, 1]
class scale_tensor(object):
    def __call__(self, sample):
        # torch image: C X H X W
        sample = sample.type(torch.float32)
        sample = 2 * sample / 1 - 1
        return sample


class unscale_tensor(object):
    def __call__(self, sample):
        # torch image: C X H X W
        sample = (sample+1)*1 / 2
        return sample


class MemeDataset(Dataset):
    def __init__(self, memes_dir, templates_dir=None, scale=None, return_meme_name=False, num_imgs=None):
        self.return_meme_name = return_meme_name
        self.templates_dir = templates_dir
        if templates_dir:
            self.templates_names = glob.glob1(templates_dir,"*.jpg") + glob.glob1(templates_dir,".png")

        self.memes_dir = memes_dir
        self.memes_names = glob.glob1(memes_dir,"*.jpg")
        self.scale = scale
        if self.scale:
            self.scale = scale_tensor()

    def __len__(self):
        return len(self.memes_names)

    def __getitem__(self, idx):
        meme_path = os.path.join(self.memes_dir, self.memes_names[idx])
        meme = read_image(meme_path)
        if self.scale:
            meme = self.scale(meme)
        if self.templates_dir:
            template_name = self.memes_names[idx].split("_")[0]  # retrieve template's name from meme's name
            template_filename = glob.glob1(self.templates_dir,f"{template_name}.*")[0]
            template_path = os.path.join(self.templates_dir, template_filename)
            template = read_image(template_path) / 255
            if self.scale:
                template = self.scale(template)
            if self.return_meme_name:
                return meme, template, self.memes_names[idx]
            return meme, template
        if self.return_meme_name:
            return meme, -1, self.memes_names[idx]
        return meme, -1
