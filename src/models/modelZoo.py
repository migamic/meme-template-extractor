import torch
import torch.nn as nn


class conv_block(nn.Module):
    def __init__(self,ch_in,ch_out):
        super(conv_block,self).__init__()
        self.conv = nn.Sequential(
            nn.Conv2d(ch_in, ch_out, kernel_size=3,stride=1,padding=1,bias=True),
            nn.BatchNorm2d(ch_out,track_running_stats=False),
            nn.ReLU(inplace=True),
            nn.Conv2d(ch_out, ch_out, kernel_size=3,stride=1,padding=1,bias=True),
            nn.BatchNorm2d(ch_out,track_running_stats=False),
            nn.ReLU(inplace=True)
        )

    def forward(self,x):
        x = self.conv(x)
        return x


# This block upsamples the input
class up_conv(nn.Module):
    def __init__(self,ch_in,ch_out):
        super(up_conv,self).__init__()
        self.up = nn.Sequential(
            nn.Upsample(scale_factor=2),
            nn.Conv2d(ch_in,ch_out,kernel_size=3,stride=1,padding=1,bias=True),
		    nn.BatchNorm2d(ch_out,track_running_stats=False),
			nn.ReLU(inplace=True)
        )

    def forward(self,x):
        x = self.up(x)
        return x


# This block performs upsampling through a transposed convolution
class up_Tr_conv(nn.Module):
    def __init__(self,ch_in,ch_out):
        super(up_Tr_conv,self).__init__()
        self.up = nn.Sequential(
            nn.ConvTranspose2d(ch_in,ch_out,kernel_size=4,stride=2,padding=1,bias=True),
		    nn.BatchNorm2d(ch_out,track_running_stats=False),
			nn.ReLU(inplace=True)
        )

    def forward(self,x):
        x = self.up(x)
        return x


class conv_1x1(nn.Module):
    def __init__(self,ch_1Layer,output_ch, activation="sigmoid"):
        super(conv_1x1,self).__init__()
        self.activation = activation
        if self.activation == "sigmoid":
            self.m = nn.Sigmoid()
        self.conv = nn.Sequential(
            nn.Conv2d(ch_1Layer,output_ch,kernel_size=1,stride=1,padding=0)
        )

    def forward(self,x):
        x = self.conv(x)
        if self.activation == "sigmoid":
            x = self.m(x)
        return x


# Unet that uses the up_conv block for upsampling
class U_Net_Up(nn.Module):
    def __init__(self):
        super(U_Net_Up,self).__init__()

    def build_net(self, img_ch=3, output_ch=3, Layers=5, ch_1Layer=16, activation="sigmoid"):
        self.Maxpool = nn.MaxPool2d(kernel_size=2,stride=2)
        self.Convs = nn.ModuleList([conv_block(ch_in=img_ch,ch_out=ch_1Layer)])
        self.Convs.extend([conv_block(ch_in=ch_1Layer * pow(2,x-1),ch_out=ch_1Layer * pow(2,x)) for x in range(1,Layers)])
        self.Up = nn.ModuleList([up_conv(ch_in=ch_1Layer * pow(2,x+1),ch_out=ch_1Layer * pow(2,x)) for x in range(Layers-2,-1,-1)])
        self.Up_Convs = nn.ModuleList([conv_block(ch_in=ch_1Layer * pow(2,x+1),ch_out=ch_1Layer * pow(2,x)) for x in range(Layers-2,-1,-1)])
        self.Conv_1x1 = conv_1x1(ch_1Layer, output_ch, activation=activation)

    def forward(self,x):
        # encoding path
        x = [x]
        for i, conv in enumerate(self.Convs):
            xaux = x[-1]
            xaux = self.Maxpool(xaux) if i > 0 else xaux
            xaux = conv(xaux)
            x.append(xaux)
        del x[0]
        x.reverse()
        uaux = x.pop(0)

        for i in range(len(self.Up)):
            uaux = self.Up[i](uaux)
            uaux = torch.cat((x.pop(0),uaux),dim=1)
            uaux = self.Up_Convs[i](uaux)

        uaux = self.Conv_1x1(uaux)
        return uaux


# Unet that uses the up_Tr_conv block for upsampling
class U_Net_Tr(nn.Module):
    def __init__(self):
        super(U_Net_Tr,self).__init__()

    def build_net(self, img_ch=3, output_ch=3, Layers=5, ch_1Layer=16, activation="sigmoid"):
        self.Maxpool = nn.MaxPool2d(kernel_size=2,stride=2)
        
        self.Convs = nn.ModuleList([conv_block(ch_in=img_ch,ch_out=ch_1Layer)])
        self.Convs.extend([conv_block(ch_in=ch_1Layer * pow(2,x-1),ch_out=ch_1Layer * pow(2,x)) for x in range(1,Layers)])
        
        self.Up = nn.ModuleList([up_Tr_conv(ch_in=ch_1Layer * pow(2,x+1),ch_out=ch_1Layer * pow(2,x)) for x in range(Layers-2,-1,-1)])
        
        self.Up_Convs = nn.ModuleList([conv_block(ch_in=ch_1Layer * pow(2,x+1),ch_out=ch_1Layer * pow(2,x)) for x in range(Layers-2,-1,-1)])
        
        self.Conv_1x1 = nn.Conv2d(ch_1Layer,output_ch,kernel_size=1,stride=1,padding=0)


    def forward(self,x):
        x = [x]
        for i, conv in enumerate(self.Convs):
            xaux = x[-1]
            xaux = self.Maxpool(xaux) if i > 0 else xaux
            xaux = conv(xaux)
            x.append(xaux)
        del x[0]
        x.reverse()
        uaux = x.pop(0)

        for i in range(len(self.Up)):
            uaux = self.Up[i](uaux)
            uaux = torch.cat((x.pop(0),uaux),dim=1)
            uaux = self.Up_Convs[i](uaux)

        return self.Conv_1x1(uaux)
