import os
import argparse

import torch
from torchvision.utils import save_image
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.autograd import Variable

import utils
import modelZoo

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def main(args):
    ## set up model/ load pretrained model
    model = getattr(modelZoo, args.model)()
    model.build_net() 
    loaded_state = torch.load(args.checkpoint, map_location=lambda storage, loc: storage)
    model.load_state_dict(loaded_state['state_dict'], strict=False)
    model = model.eval()
    model.to(device)
    criterion = nn.L1Loss()
    ## DONE set up model/ load pretrained model

    ## load/prepare data from external files
    memes_dataset = utils.MemeDataset(args.memes_dir, args.templates_dir, return_meme_name=True, scale=args.scale)
    test_loader = DataLoader(memes_dataset, batch_size=args.batch_size, shuffle=False)
    os.makedirs(args.output_dir, exist_ok=True)  # create directory to store output memes if it doesn't exist
    ## DONE load data

    ## pass loaded data into model for testing
    unscale = utils.unscale_tensor()  # unscale image to visualize
    testLoss = 0
    for idx, batch in enumerate(test_loader):
        local_batch, local_templates, local_meme_names = Variable(batch[0].float()).to(device), Variable(batch[1].float()).to(device), batch[2]
        output = model(local_batch)
        for i in range(output.shape[0]):
            img_i = output[i]   # ignore batch size dimension
            if args.unscale:  # unscale model's outputs
                img_i = unscale(img_i)
            save_image(img_i, os.path.join(args.output_dir, "predicted_"+local_meme_names[i]))
        if args.templates_dir:
            loss = criterion(output, local_templates)
            testLoss += loss.item() * args.batch_size
        if args.batch_size * (idx+1) >= args.num_imgs:  # only process args.num_imgs images
            break

    ## DONE pass loaded data into testing
    if args.templates_dir:
        print(">>> TOTAL ERROR: ", testLoss)
        print('----------------------------------')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--templates_dir', type=str, default="", help='path to directory where inference templates are stored')
    parser.add_argument('--memes_dir', type=str, default="./", help='path to directory where inference memes are stored')
    parser.add_argument('--output_dir', type=str, default="results", help='path to directory where output memes should be stored')
    parser.add_argument('--model', type=str, default="U_Net_Up", help='name of the model class in modelZoo.py')
    parser.add_argument('--checkpoint', type=str, default="models/lastCheckpoint.pth" , help='path to trained model')
    parser.add_argument('--tag', type=str, default='', help='prefix for naming purposes')
    parser.add_argument('--batch_size', type=int, default=16, help='batch size for inference')
    parser.add_argument('--num_imgs', type=int, default=16, help='number for predicted images')
    parser.add_argument('--scale', action="store_true", help='whether to scale the input of the model')
    parser.add_argument('--unscale', action="store_true", help='whether to unscale the output of the model')

    args = parser.parse_args()
    if args.templates_dir == "":  # No ground truth
        args.templates_dir = None
    print(args)
    main(args)
