import os
import shutil
import argparse
import numpy as np
import matplotlib as mpl
mpl.rcParams['axes.grid'] = False
import matplotlib.pyplot as plt
plt.style.use("ggplot")

import torch
import torch.nn as nn
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import Dataset, DataLoader
from torch.autograd import Variable
#from torchvision import transforms, utils

import utils
import modelZoo
import piq

lastCheckpoint = ""

loss = {
    'L1':nn.L1Loss(),
    'SSIM':piq.SSIMLoss(data_range=1.),
    'BRISQUE':piq.BRISQUELoss(data_range=1.),
    'CONTENT':piq.ContentLoss(feature_extractor="vgg16", layers=("relu3_3",)),
    'DISTS':piq.DISTS(),
    'DSS':piq.DSSLoss(data_range=1.),
    'FSIM':piq.FSIMLoss(data_range=1.),
    'GMSD':piq.GMSDLoss(data_range=1.),
    'HaarPSI':piq.HaarPSILoss(data_range=1.),
    'LPIPS':piq.LPIPS(),
    'MDSI':piq.MDSILoss(data_range=1.),
    'MSSSIM':piq.MultiScaleSSIMLoss(data_range=1.),
    'MSGMSD':piq.MultiScaleGMSDLoss(data_range=1.),
    'PieAPP':piq.PieAPP(),
    'STYLE':piq.StyleLoss(feature_extractor="vgg16", layers=("relu3_3",)),
    'TV':piq.TVLoss(),
    'VIF':piq.VIFLoss(sigma_n_sq=2.0, data_range=1.),
    'VSI':piq.VSILoss(data_range=1.),
    'SRSIM':piq.SRSIMLoss(data_range=1.),
}

def main(args):
    ## variables
    learning_rate = args.learning_rate
    device = torch.device("cuda:0" if not args.cpu and torch.cuda.is_available() else "cpu")
    currBestLoss = 1e3
    rng = utils.set_seed()
    ## DONE variables

    ## set up model
    generator = getattr(modelZoo, args.model)()
    generator.build_net()
    if args.checkpoint != '':
        loaded_state = torch.load(args.model_path + args.checkpoint, map_location=lambda storage, loc: storage)
        generator.load_state_dict(loaded_state['state_dict'], strict=False)
    generator.to(device)
    if args.loss in loss:
        criterion = loss[args.loss]
    else:
        raise Exception(f'unknown loss {args.loss}')
    optimizer = torch.optim.Adam(generator.parameters(), lr=learning_rate, weight_decay=1e-5)
    generator.train()

    ## load data
    memes_dataset = utils.MemeDataset(args.memes_dir, args.templates_dir, scale=args.scale)
    train_size = int(0.7 * len(memes_dataset))
    val_size = len(memes_dataset) - train_size
    train_dataset, val_dataset = torch.utils.data.random_split(memes_dataset, [train_size, val_size])
    train_loader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True)
    val_loader = DataLoader(val_dataset, batch_size=args.batch_size, shuffle=False)
    ## DONE load data

    ## setup results logger
    train_log_dir = 'logs/train/' + args.experiment_name
    val_log_dir   = 'logs/val/' + args.experiment_name
    train_summary_writer = SummaryWriter(train_log_dir)
    val_summary_writer   = SummaryWriter(val_log_dir)
    if not os.path.exists('models'):
        os.makedirs('models')
    ## DONE setup logger 

    ## training job
    prev_save_epoch = 0
    for epoch in range(args.num_epochs):
        args.epoch = epoch
        if epoch > 100 and (epoch - prev_save_epoch) > args.patience:
            print('early stopping at:', epoch)
            break

        # if epoch > 0 and epoch % 3 == 0:
        #     train_discriminator(args, rng, generator, discriminator, gan_criterion, d_optimizer, train_X, train_Y, train_ims=train_ims)
        # else:
        train_generator(args, rng, generator, train_loader, criterion, optimizer, epoch, device, train_summary_writer)
        currBestLoss = val_generator(args, generator, val_loader, criterion, optimizer, currBestLoss, epoch, device, val_summary_writer)
    shutil.copyfile(lastCheckpoint, args.model_path + "/lastCheckpoint.pth")  # name last checkpoint as "lastCheckpoint.pth"


## training generator function
def train_generator(args, rng, generator, train_loader, criterion, optimizer, epoch, device, train_summary_writer):
    generator.train()
    trainLoss = 0
    for idx, batch in enumerate(train_loader):
        local_batch, local_templates = Variable(batch[0].float()).to(device), Variable(batch[1].float()).to(device)
        output = generator(local_batch)
        loss = criterion(output, local_templates)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        trainLoss += loss.item()
        if idx % args.log_step == 0:
            print('Epoch [{}/{}], Step [{}/{}]'.format(args.epoch+1, args.num_epochs, idx, len(train_loader)))
    trainLoss /= len(train_loader)
    print('Epoch [{}/{}], Tr. Loss: {:.4f}, Tr. Perplexity: {:5.4f}'.format(args.epoch+1, args.num_epochs,
                                                                              trainLoss, np.exp(trainLoss)))
    # Save data to tensorboard                             
    train_summary_writer.add_scalar('loss', trainLoss / (len(train_loader) * args.batch_size), global_step=epoch)


## validating generator function
def val_generator(args, generator, val_loader, criterion, optimizer, currBestLoss, epoch, device, val_summary_writer):
    valLoss = 0
    generator.eval()
    for idx, batch in enumerate(val_loader):
        local_batch, local_templates = Variable(batch[0].float()).to(device), Variable(batch[1].float()).to(device)
        output = generator(local_batch)
        loss = criterion(output, local_templates)
        valLoss += loss.item() * args.batch_size
    val_summary_writer.add_scalar('loss', valLoss / (len(val_loader) * args.batch_size), global_step=epoch)

    valLoss /= len(val_loader) * args.batch_size
    print('Epoch [{}/{}], Val. Loss: {:.4f}, Val. Perplexity: {:5.4f}'.format(args.epoch+1, args.num_epochs,
                                                                              valLoss, np.exp(valLoss)))
    print('----------------------------------')
    if valLoss < currBestLoss:
        prev_save_epoch = args.epoch
        checkpoint = {'epoch': args.epoch,
                      'state_dict': generator.state_dict(),
                      'g_optimizer': optimizer.state_dict()}
        fileName = args.model_path + '/{}_checkpoint_e{}_loss{:.4f}.pth'.format(args.tag, args.epoch, valLoss)
        torch.save(checkpoint, fileName)
        currBestLoss = valLoss
        global lastCheckpoint
        lastCheckpoint = fileName
    return currBestLoss


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--templates_dir', type=str, required=True, help='path to directory where templates are stored')
    parser.add_argument('--memes_dir', type=str, required=True, help='path to directory where memes are stored')
    parser.add_argument('--num_epochs', type=int, default=100, help='number of training epochs')
    parser.add_argument('--batch_size', type=int, default=64, help='batch size for training')
    parser.add_argument('--model', type=str, default="U_Net_Up", help='name of the model class in modelZoo.py')
    parser.add_argument('--learning_rate', type=float, default=1e-3, help='learning rate for training')
    parser.add_argument('--model_path', type=str, default="./models" , help='path for saving trained models')
    parser.add_argument('--scale', action="store_true", help='whether to scale the input to the model')
    parser.add_argument('--log_step', type=int , default=20, help='step size for printing log info')
    parser.add_argument('--patience', type=int , default=20, help='number of epochs to wait before early stopping')
    parser.add_argument('--tag', type=str, default='', help='prefix for naming purposes')
    parser.add_argument('--checkpoint', type=str, default='', help='Path to the model state used to start training')
    parser.add_argument('--loss', type=str, default='L1', help='loss for training (L1 or SSIM)')
    parser.add_argument('--cpu', action='store_true', help='use this command to force cpu training')
    parser.add_argument('--experiment_name', type=str, default='exp1', help='experiment identifier for tensorboard logging')

    args = parser.parse_args()
    print(args)
    main(args)
