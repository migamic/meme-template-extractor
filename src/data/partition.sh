#!/bin/bash

# Splits the dataset into training and test partitions with the given probability


# Parameters
root_folder='./dataset/data'
train_folder="$root_folder/train"
test_folder="$root_folder/test"
# Each folder will include a 'memes' and 'templates' folder separately
seed=1234


# Settings, as defined in the flags
prob=0.8   # Probability of an image belonging to the train partition (i.e. aprox the proportion of train data)


# Parse flags
while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "Script to split the dataset into training and test partitions"
            echo
            echo "Options:"
            echo "-h|--help : Show this help message"
            echo "-p|--probability : Probability (i.e. aprox proportion) of an image belonging to the train partition"
            exit 0
            ;;
        -p|--probability)
            if test $# -gt 0; then
                prob="$2"
            else
                echo "No probability value specified"
                exit 1
            fi
            shift
            ;;

        *)
            break
    esac
done


# Create folders
mkdir "$train_folder"
mkdir "$test_folder"
mkdir "$train_folder/memes"
mkdir "$train_folder/templates"
mkdir "$test_folder/memes"
mkdir "$test_folder/templates"

# Copy the whole templates directory to both train and test
cp -r "$root_folder/templates" "$train_folder"
mv "$root_folder/templates" "$test_folder"


RANDOM=$seed
# For each meme, put it in train_folder with probability 'prob'. Else in test_folder
for meme_name in $root_folder/memes/*
do
    rand_num=$((1 + $RANDOM % 10))
    if (( $(echo "$rand_num > $prob*10" | bc -l) )); then
        mv "$meme_name" "$test_folder/memes"
    else
        mv "$meme_name" "$train_folder/memes"
    fi
done

rm -r "$root_folder/memes" # The folder is now empty


echo "Done"

# Final stats
train_memes=$(ls "$train_folder/memes" | wc -l)
test_memes=$(ls "$test_folder/memes" | wc -l)
total=$((${train_memes} + ${test_memes}))

echo "# of train images : $train_memes ($((${train_memes} * 100 / ${total}))%)"
echo "# of test images  : $test_memes ($((${test_memes} * 100 / ${total}))%)"
