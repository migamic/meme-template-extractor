#!/bin/bash

# Preprocesses a single image. Scales and adds padding if needed.

# Requires imagemagick


# Parameters
memes_folder='./dataset/data/memes'
templ_folder='./dataset/data/templates'


# Settings, as defined in the flags
padding=false
custom_res=false


# Parse flags
while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "Script to preprocess all memes to match their templates"
            echo
            echo "Usage: preprocess.sh <options> <image file>"
            echo "Options:"
            echo "-h|--help : Show this help message"
            echo "-s|--size (<value>): Resolution (both axes) of the square output image"
            echo "-p|--padding : Add zero-padding to preserve aspect ratio"
            echo "For more info look at the beginning of the code"
            exit 0
            ;;
        -p|--padding)
            padding=true
            shift
            ;;
        -s|--size)
            custom_res=true
            shift
            if test $# -gt 0; then
                out_res="$1"
                case $out_res in
                    ''|*[!0-9]*) echo "Invalid resolution value"; exit 1 ;;
                esac

            else
                echo "No resolution value specified"
                exit 1
            fi
            shift
            ;;

        *)
            break
    esac
done

image_file=$1


if [ "$custom_res" = true ]; then
    out_res="$out_res"x"$out_res"
else
    # Get resolution from 'identify' output
    idnfy_out="$(identify "$image_file")"
    splitted=($idnfy_out)
    out_res=${splitted[2]}
fi


# Zero-pad if required
if [ "$padding" = true ]; then
    # Resize using imagemagick's 'convert'. Changes aspect ratio if necessary
    convert "$image_file" -resize "$out_res" "$image_file"
    convert "$image_file" -gravity Center -background black -extent "$out_res" "$image_file"
else
    out_res="$out_res"! # The "!" symbol tells imagemagick to stretch the image when scaling
    convert "$image_file" -resize "$out_res" "$image_file"
fi

# Also convert to sRGB in the rare case that the original was in grayscale
convert "$image_file" -colorspace sRGB -type truecolor "$image_file"
