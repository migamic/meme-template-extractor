#!/bin/bash

# Preprocesses the images of the dataset. Scales and adds padding if needed.
# Relies on the img_img_preprocess.sh script

# Requires imagemagick


# Parameters
memes_folder='./dataset/data/memes'
templ_folder='./dataset/data/templates'


# Settings, as defined in the flags
padding=false
custom_res=false


# Parse flags
while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "Script to preprocess all memes to match their templates"
            echo
            echo "Options:"
            echo "-h|--help : Show this help message"
            echo "-s|--size (<value> or 'max'): Resolution (both axes) of the square output image"
            echo "-p|--padding : Add zero-padding to preserve aspect ratio"
            echo "For more info look at the beginning of the code"
            exit 0
            ;;
        -p|--padding)
            padding=true
            shift
            ;;
        -s|--size)
            custom_res=true
            shift
            if test $# -gt 0; then
                out_res="$1"
            else
                echo "No resolution value specified"
                exit 1
            fi
            shift
            ;;

        *)
            break
    esac
done


if [ "$custom_res" = true ]; then
    if [ "$out_res" = max ]; then
        # Get maximum resolution
        out_res=0 # We only care about the max in both axes

        # Loop through all templates
        for template_file in $(ls $templ_folder)
        do
            # Get resolution from 'identify' output
            idnfy_out="$(identify "$templ_folder/$template_file")"
            splitted=($idnfy_out)
            templ_res=${splitted[2]}
            IFS='x' read -ra parts <<< "$templ_res" # Split by 'x' char
            max_dim=$((${parts[0]}>${parts[1]} ? ${parts[0]} : ${parts[1]})) # Get max axis
            out_res=$(($max_dim>$out_res ? $max_dim : $out_res)) # Update out_res if new max found
        done

        echo "Max template resolution computed"
    fi
    out_res="$out_res"x"$out_res"
fi


echo "Starting preprocess"

# Loop through all templates
for template_file in $(ls $templ_folder)
do 
    # Get template name
    template_name="${template_file%%.*}"        # Remove .jpg extension
    echo Processing "$template_name"

    if [ "$custom_res" = false ]; then
        # Get resolution from 'identify' output
        idnfy_out="$(identify "$templ_folder/$template_file")"
        splitted=($idnfy_out)
        out_res=${splitted[2]}
    fi

    if [ "$padding" = false ]; then
        out_res="$out_res"! # The "!" symbol tells imagemagick to stretch the image when scaling
    fi


    IFS='x' read -ra parts <<< "$out_res" # Split by 'x' char
    out_res=${parts[0]}


    # Loop through all memes with this template
    for meme_file in $(ls "$memes_folder/$template_name"*)
    do

        # Use the img_preprocess script with the adequate arguments
        if [ "$padding" = true ]; then
            ./img_preprocess.sh -p -s $out_res $meme_file
        else
            ./img_preprocess.sh -s $out_res $meme_file
        fi

    done

    # Also scale the template if necessary
    if [ "$custom_res" = true ]; then

        # Use the img_preprocess script with the adequate arguments
        if [ "$padding" = true ]; then
            ./img_preprocess.sh -p -s $out_res "$templ_folder/$template_file"
        else
            ./img_preprocess.sh -s $out_res "$templ_folder/$template_file"
        fi
    fi

done


echo "Done"
