import git
import os
import json
from werkzeug.utils import secure_filename
import requests
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-n','--numMemes', default=None, help='Number of memes downloaded for each template')
parser.add_argument('-r','--replace', action='store_true', help='Replace memes if exist')

args = parser.parse_args()
nm = args.numMemes
replace = args.replace

DatasetGit = './dataset'
gitDir = '/'.join([DatasetGit,'ImgFlip575K_Dataset'])
dataDir = '/'.join([gitDir,'dataset'])
memeDir = '/'.join([dataDir,'memes'])
templateDir = '/'.join([dataDir,'templates'])

data = '/'.join([DatasetGit,'data'])
dataMeme = '/'.join([DatasetGit,'data','memes'])
dataTemp = '/'.join([DatasetGit,'data','templates'])


def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def datasetDir():
    ensure_dir(data)
    ensure_dir(dataMeme)
    ensure_dir(dataTemp)


def log(text):
    print(text)

def downloadImage(url, path):
    if not replace and os.path.exists(path):
        return True
    response = requests.get(url)

    if response.status_code == 200:
        file = open(path, "wb")
        file.write(response.content)
        file.close()
        return True
    else:
        log(f"meme {path.split('/')[-1]} not saved, requests failed with code: {response.status_code}")
        return False

def downloadTemplates():
    log('Downloading Templates')
    for filename in os.listdir(templateDir):
        if filename.endswith(".json"):
            log(f'downloading {filename[:-5]} template')
            t = open('/'.join([templateDir,filename]))
            templJson = json.load(t)
            t.close()
            if 'template_url' in templJson:
                url = templJson['template_url']
                fileType = '.'+url.split('.')[-1]
                file = secure_filename(filename[:-5]+fileType)

                downloadImage(url,dataTemp+'/'+file)
                log('Done!')
            else:
                log('impossible to download this template')
    log('finished downloading templates')

def downloadAllMemes():
    log('Downloading Memes')
    for filename in os.listdir(memeDir):
        if filename.endswith(".json"):
            m = open('/'.join([memeDir,filename]))
            memeJson = json.load(m)
            m.close()
            downloadMemes(memeJson,filename[:-5])
    log('Finished Downloading Memes')

def downloadMemes(Memes, templateName):
    log(f'downloading {templateName} memes')
    count = 1
    for meme in Memes:
        if nm is not None and count > int(nm):
            break
        if 'url' in meme:
            url = meme['url']
            fileType = '.'+url.split('.')[-1]
            file = secure_filename(templateName+f'_{count:04d}'+fileType)
            if downloadImage(url,dataMeme+'/'+file):
                count += 1
        else:
            log(f'impossible to download this meme')
    log(f'Done downloading {templateName} memes')
    

path = DatasetGit + '/' + 'ImgFlip575K_Dataset'
if replace or not os.path.exists(path):
    log('creating Git dir')
    ensure_dir(DatasetGit)
    log('Done!')

    log('cloning Git rep')
    git.Git(DatasetGit).clone("https://github.com/schesa/ImgFlip575K_Dataset.git")
    log('Done!')

    log('creating data dirs')
    datasetDir()
    log('Done!')

downloadTemplates()
downloadAllMemes()
