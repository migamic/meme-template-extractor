# Meme template extractor
End-to-end Deep Learning model that cleans the text off a meme image to obtain the original template.


### Authors
* [Anxo Lois Pereira Canovas](https://gitlab.com/ganxito)
* [Jaume Ros Alonso](https://gitlab.com/migamic)
* Álvaro Francesc Budría Fernández

![Demo image](imgs/demoImage.png "Demo image")

The model is based un [U-Net](https://arxiv.org/abs/1505.04597); more details may be found on the [Architecture](#architecture) section.

It is trained on the [ImgFlip575K Memes Dataset](https://github.com/schesa/ImgFlip575K_Dataset). Although it is trained only on 100 different templates, its results generalize to other images, as explained on the [Results](#results) section.


## Repository outline
* **data**: Small sample data set, to show the structure.
* **imgs**: Example images and those used in the README.
* **models**: Pretrained models ready for inference.
* **src**: Code for the project. Split into:
    * **src/data**: Code for downloading and preprocessing the data set.
    * **src/models**: Code for the models (training and inference).

## Installation
### Requirements
In order to install all required modules and their dependencies use:
```
pip install -r requirements.txt
```
### Downloading the data
To download the dataset use the ```downloader.py``` file. To do so use:
```
python downloader.py
```
If you want to test the program first or use a reduced dataset, you can use the *-n* or *-numMemes* to do so, this will download only the specified number of memes for each template.

use the *-h* flag for more help.

## Usage
### Preprocessing the data
The image/dataset preprocessing is done through three scripts:
* `dst_preprocess.sh` preprocesses the dataset just downloaded (runs the `img_preprocess.sh` in batch). Our recommended use is `dst_preprocess.sh -s 256 -p`; it needs to be run on the root directory of the repository (where the `dataset` folder is).
* `img_preprocess.sh` preprocesses a single image. Use this to preprocess your own images prior to inference.
* `partition.sh` splits the dataset into training and test partitions. It should be run after the `dst_preprocess` script. The ratio of train/test data is done randomly with a given probability, which can be set with the `-p` parameter, between 0 and 1, which represents the probability of an image belonging to the train dataset. There is a seed parameter to allow for reproducibility.

For all scripts use the `-h` option to see all possible flags. Some additional parameters, such as the location of the data directories, can be modified at the beginning of the code. Note that for training you should only need the `dst_preprocess` and `partition` scripts, while for inference you should only need the `img_preprocess` script.
> Warning: there is no automatic way of reversing the actions performed by the preprocessing and partition scripts. Consider having a backup copy of the dataset to avoid having to download it next time.
### Training the model
### Inference

## Architecture
![Architecture image](imgs/arch.png "Architecture image")

The main model is a UNet-like neural network built with [PyTorch](https://pytorch.org/). The downsampling allows for multiresolution processing of the image, while the residual connections are useful to maintain the sharpness of the image while upsampling.

Since the model only uses 2D convolutions, the original resolution *n* is flexible: higher values will produce higher quality images but have higher memory requirements.

The model is accompanied by pre- and post-processing scripts which can be run separately (note that the latter is not yet available).

## Results

> Note: This project is still under development. We expect better results in future versions.

When inputting templates used during training, the model is able to remove most of the text producing almost unnoticeable distortion in the image.
![Domain image](imgs/domainExample.png "Domain image")

When using images very different from any of the ones the model has seen (out-of-domain), the results are also acceptable for many purposes. In most of the cases it correctly identifies the text and removes it, but the reconstruction of the image underneath (unknown to the model) is not always perfect; however, if the purpose is to add some new text using the template, the performance should be enough.
![Out of domain 1](imgs/OutOfDomain1.png "Out of domain images 1")
![Out of domain 2](imgs/OutOfDomain2.png "Out of domain images 2")
